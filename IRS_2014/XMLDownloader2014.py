#!/usr/local/bin/python
import pandas as pd
import matplotlib.pyplot as plt
import sys
import os
import urllib

#  pandas.io.common.CParserError: Error tokenizing data. C error: Expected 9 fields in line 39569, saw 10
#  Pandas ran into the above error which required some data cleaning, theres a 
#  inserted "," into the dataset that causes issues


# File paths for each year 
IRS_2011 = '/Users/brianbroeking/XML/index_2011.csv'
IRS_2012 = '/Users/brianbroeking/XML/index_2012.csv'
IRS_2013 = '/Users/brianbroeking/XML/index_2013.csv'
IRS_2014 = '/Users/brianbroeking/XML/index_2014.csv'
IRS_2015 = '/Users/brianbroeking/XML/index_2015.csv'

# Place each data set in Pandas Data Frame

IRS_2014DF = pd.read_csv(IRS_2014)
	
### Downloading the 2014 IRS XMLs

for OBJ_ID, TAX_NAME in zip(IRS_2014DF['OBJECT_ID'], IRS_2014DF['TAXPAYER_NAME']):
	OBJ_ID = str(OBJ_ID)
	TAX_NAME = TAX_NAME.replace(" ", "")
	url = 'https://s3.amazonaws.com/irs-form-990/' + OBJ_ID + '_public.xml'
	filename = TAX_NAME + '.xml'
	download = urllib.urlretrieve(url, filename)
	download






