#!/usr/local/bin/python
import pandas as pd
import matplotlib.pyplot as plt
import sys
import os
import urllib

# File paths for each year 
IRS_2011 = '/Users/brianbroeking/XML/index_2011.csv'
IRS_2012 = '/Users/brianbroeking/XML/index_2012.csv'
IRS_2013 = '/Users/brianbroeking/XML/index_2013.csv'
IRS_2014 = '/Users/brianbroeking/XML/index_2014.csv'
IRS_2015 = '/Users/brianbroeking/XML/index_2015.csv'

# Place each data set in Pandas Data Frame
# IRS_2011DF = pd.read_csv(IRS_2011)
# IRS_2012DF = pd.read_csv(IRS_2012)
# IRS_2013DF = pd.read_csv(IRS_2013)
# IRS_2014DF = pd.read_csv(IRS_2014)
IRS_2015DF = pd.read_csv(IRS_2015)

### Downloading the 2015 IRS XMLs

for OBJ_ID, TAX_NAME in zip(IRS_2015DF['OBJECT_ID'], IRS_2015DF['TAXPAYER_NAME']):
	OBJ_ID = str(OBJ_ID)
	TAX_NAME = TAX_NAME.replace(" ", "")
	url = 'https://s3.amazonaws.com/irs-form-990/' + OBJ_ID + '_public.xml'
	filename = TAX_NAME + '.xml'
	path = '/Users/brianbroeking/XML/IRS_2015'
	download = urllib.urlretrieve(url, filename)
	download
	
# ### Downloading the 2014 IRS XMLs

# for OBJ_ID, TAX_NAME in zip(IRS_2014DF['OBJECT_ID'], IRS_2014DF['TAXPAYER_NAME']):
# 	OBJ_ID = str(OBJ_ID)
# 	TAX_NAME = TAX_NAME.replace(" ", "")
# 	url = 'https://s3.amazonaws.com/irs-form-990/' + OBJ_ID + '_public.xml'
# 	filename = TAX_NAME + '.xml'
# 	path = '/Users/brianbroeking/XML/IRS_2014'
# 	download = urllib.urlretrieve(url, filename)
# 	download

# ### Downloading the 2013 IRS XMLs

# for OBJ_ID, TAX_NAME in zip(IRS_2013DF['OBJECT_ID'], IRS_2013DF['TAXPAYER_NAME']):
# 	OBJ_ID = str(OBJ_ID)
# 	TAX_NAME = TAX_NAME.replace(" ", "")
# 	url = 'https://s3.amazonaws.com/irs-form-990/' + OBJ_ID + '_public.xml'
# 	output = TAX_NAME + '.xml'
# 	path = '/Users/brianbroeking/XML/IRS_2013'
# 	download = urllib.urlretrieve(url, output)
# 	download

# ### Downloading the 2012 IRS XMLs

# for OBJ_ID, TAX_NAME in zip(IRS_2012DF['OBJECT_ID'], IRS_2012DF['TAXPAYER_NAME']):
# 	OBJ_ID = str(OBJ_ID)
# 	TAX_NAME = TAX_NAME.replace(" ", "")
# 	url = 'https://s3.amazonaws.com/irs-form-990/' + OBJ_ID + '_public.xml'
# 	output = TAX_NAME + '.xml'
# 	path = '/Users/brianbroeking/XML/IRS_2012'
# 	download = urllib.urlretrieve(url, output)
# 	download

# ### Downloading the 2011 IRS XMLs

# for OBJ_ID, TAX_NAME in zip(IRS_2011DF['OBJECT_ID'], IRS_2011DF['TAXPAYER_NAME']):
# 	OBJ_ID = str(OBJ_ID)
# 	TAX_NAME = TAX_NAME.replace(" ", "")
# 	url = 'https://s3.amazonaws.com/irs-form-990/' + OBJ_ID + '_public.xml'
# 	output = TAX_NAME + '.xml'
# 	path = '/Users/brianbroeking/XML/IRS_2011'
# 	download = urllib.urlretrieve(url, output)
# 	download









